#################################################################################
#                                 Compute instances                             #
#################################################################################
########################################
#         Instance template            #
########################################
resource "random_id" "demo_dashboard_name_suffix" {
  byte_length = 4
}

resource "google_compute_instance_template" "demo_dashboard_template" {
  name        = "${var.instance_group_demo_dashboard_name}-${random_id.demo_dashboard_name_suffix.hex}"
  description = "This template is used to create app server instances."

  tags = [var.demo_dashboard_app_target_tags,var.demo_dashboard_http_https_target_tags, var.demo_dashboard_ssh_target_tags]

  metadata_startup_script = "sudo apt-get -y install unzip && sudo apt-get -y install python-pip && sudo mkdir /flask && cd /flask && sudo gsutil cp gs://${var.app_engine_code_bucket_name}/${var.app_engine_code_bucket_object_1_name} . && sudo unzip flask.zip && sudo pip install -r requirements.txt && sudo python runserver.py"

  instance_description = "App server instance"
  machine_type         = var.instance_demo_dashboard_type
  can_ip_forward       = false

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
  }

  // Create a new boot disk from an image
  disk {
    source_image = "debian-9"
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network = var.instance_demo_dashboard_network
    access_config {
      // Ephemeral IP
    }
  }

  service_account {
    scopes = ["cloud-platform"]  # cloud-platform = all api's (https://cloud.google.com/sdk/gcloud/reference/alpha/compute/instances/set-scopes#--scopes)
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [var.role_compute_admin_id,random_id.demo_dashboard_name_suffix,var.app_engine_code_bucket_name, var.app_engine_code_bucket_object_1_name]
}
########################################
#         Instance group               #
########################################
resource "google_compute_instance_group_manager" "demo_dashboard_instance_group_manager" {
  name               = "${var.instance_group_demo_dashboard_name}-${random_id.demo_dashboard_name_suffix.hex}"
  base_instance_name = var.instance_group_demo_dashboard_name
  zone               = var.instance_demo_dashboard_zone
  target_size        = "1"
  version {
    name              = "appserver"
    instance_template  = google_compute_instance_template.demo_dashboard_template.self_link
  }

  depends_on = [google_compute_instance_template.demo_dashboard_template,var.app_engine_code_bucket_name, var.app_engine_code_bucket_object_1_name]
}
