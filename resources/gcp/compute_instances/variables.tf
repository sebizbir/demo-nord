# Single Instances
variable "role_compute_admin_id" {}
variable "instance_demo_dashboard_type" {}
variable "instance_demo_dashboard_zone" {}
variable "instance_demo_dashboard_network" {}

variable "demo_dashboard_app_target_tags" {}
variable "demo_dashboard_http_https_target_tags" {}
variable "demo_dashboard_ssh_target_tags" {}
# Instance groups
variable "instance_group_demo_dashboard_name" {}

variable "app_engine_code_bucket_name" {}
variable "app_engine_code_bucket_object_1_name" {}
