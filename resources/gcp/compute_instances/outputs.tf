output "demo_dashboard_instance_group_manager_full" {
  value = google_compute_instance_group_manager.demo_dashboard_instance_group_manager.instance_group
}

output "demo_dashboard_instance_group_manager_id" {
  value = google_compute_instance_group_manager.demo_dashboard_instance_group_manager.id
}

output "demo_dashboard_instance_group_manager_name" {
  value = google_compute_instance_group_manager.demo_dashboard_instance_group_manager.name
}

output "demo_dashboard_instance_group_manager_zone" {
  value = google_compute_instance_group_manager.demo_dashboard_instance_group_manager.zone
}