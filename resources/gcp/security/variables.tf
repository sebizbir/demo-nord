variable "network_name" {}
variable "demo_dashboard_http_https_allow_protocol" {}
variable "demo_dashboard_http_https_allow_protocol_ports" {}
variable "demo_dashboard_http_https_target_tags" {}
variable "demo_dashboard_http_https_source_ranges" {}
variable "demo_dashboard_network_id" {}

variable "demo_dashboard_ssh_allow_protocol" {}
variable "demo_dashboard_ssh_allow_protocol_ports" {}
variable "demo_dashboard_ssh_target_tags" {}
variable "demo_dashboard_ssh_source_ranges" {}

variable "demo_dashboard_app_allow_protocol" {}
variable "demo_dashboard_app_allow_protocol_ports" {}
variable "demo_dashboard_app_target_tags" {}
variable "demo_dashboard_app_source_ranges" {}
