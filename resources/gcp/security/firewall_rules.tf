#################################################################################
#                               Firewall rules                                  #
#################################################################################
########################################
#              HTTP/HTTPS              #
########################################
resource "google_compute_firewall" "demo_dashboard_http_https" {
  name    = "demo-dashboard-http-https-firewall"
  network = var.network_name
  allow {
    protocol = var.demo_dashboard_http_https_allow_protocol
    ports    = var.demo_dashboard_http_https_allow_protocol_ports
  }
  target_tags = [var.demo_dashboard_http_https_target_tags]
  source_ranges = [var.demo_dashboard_http_https_source_ranges]

  depends_on = [var.demo_dashboard_network_id]
}
########################################
#                 SSH                  #
########################################
resource "google_compute_firewall" "demo_dashboard_ssh" {
  name    = "demo-dashboard-ssh-firewall"
  network = var.network_name
  allow {
    protocol = var.demo_dashboard_ssh_allow_protocol
    ports    = var.demo_dashboard_ssh_allow_protocol_ports
  }
  target_tags = [var.demo_dashboard_ssh_target_tags]
  source_ranges = [var.demo_dashboard_ssh_source_ranges]

  depends_on = [var.demo_dashboard_network_id]
}
########################################
#                 App                  #
########################################
resource "google_compute_firewall" "demo_dashboard_app" {
  name    = "demo-dashboard-app-firewall"
  network = var.network_name
  allow {
    protocol = var.demo_dashboard_app_allow_protocol
    ports    = var.demo_dashboard_app_allow_protocol_ports
  }
  target_tags = [var.demo_dashboard_app_target_tags]
  source_ranges = [var.demo_dashboard_app_source_ranges]

  depends_on = [var.demo_dashboard_network_id]
}
