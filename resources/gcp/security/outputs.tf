output "demo_dashboard_http_https_id" {
  value = google_compute_firewall.demo_dashboard_http_https.id
}
output "demo_dashboard_ssh_id" {
  value = google_compute_firewall.demo_dashboard_ssh.id
}
output "demo_dashboard_app_id" {
  value = google_compute_firewall.demo_dashboard_app.id
}