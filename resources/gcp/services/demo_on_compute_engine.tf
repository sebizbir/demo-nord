resource "null_resource" "demo_on_compute_engine" {
  provisioner "local-exec" {
    command = "Get-Date > plans/created_demo_on_compute_engine.txt"
    interpreter = ["PowerShell", "-Command"]
  }

  depends_on = [
    var.api_compute,
    var.role_compute_admin_id,
    var.demo_dashboard_http_https_id,
    var.demo_dashboard_ssh_id,
    var.demo_dashboard_app_id,
    var.demo_dashboard_instance_group_manager_id
  ]
}

resource "null_resource" "demo_on_compute_engine_with_nlb" {
  provisioner "local-exec" {
    command = "Get-Date > plans/created_demo_on_compute_engine_with_nlb.txt"
    interpreter = ["PowerShell", "-Command"]
  }

  depends_on = [
    var.api_compute,
    var.role_compute_admin_id,
    var.demo_dashboard_http_https_id,
    var.demo_dashboard_ssh_id,
    var.demo_dashboard_app_id,
    var.demo_dashboard_instance_group_manager_id,
    var.demo_dashboard_fwd_id,
    var.demo_dashboard_tp_id
  ]
}

resource "null_resource" "demo_on_compute_engine_with_http_lb" {
  provisioner "local-exec" {
    command = "Get-Date > plans/created_demo_on_compute_engine_with_http_lb.txt"
    interpreter = ["PowerShell", "-Command"]
  }

  depends_on = [
    var.api_compute,
    var.role_compute_admin_id,
    var.demo_dashboard_http_https_id,
    var.demo_dashboard_ssh_id,
    var.demo_dashboard_app_id,
    var.demo_dashboard_instance_group_manager_id,
    var.demo_dashboard_http_forwarding_rule_id
  ]
}


resource "null_resource" "demo_on_compute_engine_with_nlb_and_domain" {
  provisioner "local-exec" {
    command = "Get-Date > plans/created_demo_on_compute_engine_with_nlb_and_domain.txt"
    interpreter = ["PowerShell", "-Command"]
  }

  depends_on = [
    var.api_compute,
    var.role_compute_admin_id,
    var.demo_dashboard_http_https_id,
    var.demo_dashboard_ssh_id,
    var.demo_dashboard_app_id,
    var.demo_dashboard_instance_group_manager_id,
    var.demo_dashboard_fwd_id,
    var.demo_lb_a_record_id
  ]
}

resource "null_resource" "demo_on_compute_engine_with_http_lb_and_domain" {
  provisioner "local-exec" {
    command = "Get-Date > plans/created_demo_on_compute_engine_with_http_lb_and_domain.txt"
    interpreter = ["PowerShell", "-Command"]
  }

  depends_on = [
    var.api_compute,
    var.role_compute_admin_id,
    var.demo_dashboard_http_https_id,
    var.demo_dashboard_ssh_id,
    var.demo_dashboard_app_id,
    var.demo_dashboard_instance_group_manager_id,
    var.demo_dashboard_http_forwarding_rule_id,
    var.demo_lb_a_record_id
  ]
}