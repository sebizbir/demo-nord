output "terraform_service_account_email" {
  value = data.google_service_account.terraform_creator_service_account.email
}
output "role_appengine_admin_id" {
  value = google_project_iam_binding.role_appengine_admin.id
}
output "role_compute_admin_id" {
  value = google_project_iam_binding.role_compute_admin.id
}

output "api_cloudresourcemanager" {
  value = google_project_service.demo_project_manager.service
}
output "api_dns" {
  value = google_project_service.demo_project_dns.service
}
output "api_iam" {
  value = google_project_service.demo_project_iam.service
}
output "api_compute" {
  value = google_project_service.demo_project_compute.service
}
output "api_registry" {
  value = google_project_service.demo_project_registry.service
}
output "api_cloud_run" {
  value = google_project_service.demo_project_cloud_run.service
}
output "api_cloud_build" {
  value = google_project_service.demo_project_cloud_build.service
}
output "api_service_networking" {
  value = google_project_service.demo_project_service_networking.service
}
output "api_cloud_sql" {
  value = google_project_service.demo_project_cloud_sql.service
}
output "api_appengine" {
  value = google_project_service.demo_project_appengine.service
}