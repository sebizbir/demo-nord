### Identity and Access Management (IAM) API, Cloud Resource Manager API should be enabled

data "google_service_account" "terraform_creator_service_account" {
  account_id = "terraform"
  project = var.demo_project_id
}
##############   https://cloud.google.com/iam/docs/understanding-roles
####  To add roles user should initially have Security Admin role, Service Account Admin and Editor


##  Cloud SQL Admin
resource "google_project_iam_binding" "role_cloud_sql_admin" {
  project = var.demo_project_id
  role    = "roles/cloudsql.admin"
  members = [
    "serviceAccount:${data.google_service_account.terraform_creator_service_account.email}"
  ]
}
##  Compute Admin
resource "google_project_iam_binding" "role_compute_admin" {
  project = var.demo_project_id
  role    = "roles/compute.admin"
  members = [
    "serviceAccount:${data.google_service_account.terraform_creator_service_account.email}"
  ]
}
##  Compute Network Admin
resource "google_project_iam_binding" "role_compute_network_admin" {
  project = var.demo_project_id
  role    = "roles/compute.networkAdmin"
  members = [
    "serviceAccount:${data.google_service_account.terraform_creator_service_account.email}"
  ]
}
##  DNS Administrator
resource "google_project_iam_binding" "role_dns_admin" {
  project = var.demo_project_id
  role    = "roles/dns.admin"
  members = [
    "serviceAccount:${data.google_service_account.terraform_creator_service_account.email}"
  ]
}
##  Service Account User
resource "google_project_iam_binding" "role_service_account_user" {
  project = var.demo_project_id
  role    = "roles/iam.serviceAccountUser"
  members = [
    "serviceAccount:${data.google_service_account.terraform_creator_service_account.email}"
  ]
}
##  Cloud Run Admin
resource "google_project_iam_binding" "role_cloud_run_admin" {
  project = var.demo_project_id
  role    = "roles/run.admin"
  members = [
    "serviceAccount:${data.google_service_account.terraform_creator_service_account.email}"
  ]
}
##  Service Usage Admin
resource "google_project_iam_binding" "role_service_usage_admin" {
  project = var.demo_project_id
  role    = "roles/serviceusage.serviceUsageAdmin"
  members = [
    "serviceAccount:${data.google_service_account.terraform_creator_service_account.email}"
  ]
}
##  Storage Admin
resource "google_project_iam_binding" "role_storage_admin" {
  project = var.demo_project_id
  role    = "roles/storage.admin"
  members = [
    "serviceAccount:${data.google_service_account.terraform_creator_service_account.email}"
  ]
}
##  App Engine Admin
resource "google_project_iam_binding" "role_appengine_admin" {
  project = var.demo_project_id
  role    = "roles/appengine.appAdmin"
  members = [
    "serviceAccount:${data.google_service_account.terraform_creator_service_account.email}"
  ]
}


