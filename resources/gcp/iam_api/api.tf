resource "google_project_service" "demo_project_manager" {
  project = var.demo_project_id
  service = "cloudresourcemanager.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_iam_binding.role_service_usage_admin]
}

resource "google_project_service" "demo_project_dns" {
  project = var.demo_project_id
  service = "dns.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_iam_binding.role_service_usage_admin,google_project_service.demo_project_manager]
}

resource "google_project_service" "demo_project_iam" {
  project = var.demo_project_id
  service = "iam.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_iam_binding.role_service_usage_admin,google_project_service.demo_project_manager]
}

resource "google_project_service" "demo_project_compute" {
  project = var.demo_project_id
  service = "compute.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_iam_binding.role_service_usage_admin,google_project_service.demo_project_manager]
}

resource "google_project_service" "demo_project_registry" {
  project = var.demo_project_id
  service = "containerregistry.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_iam_binding.role_service_usage_admin,google_project_service.demo_project_manager]
}

resource "google_project_service" "demo_project_cloud_run" {
  project = var.demo_project_id
  service = "run.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_iam_binding.role_service_usage_admin,google_project_service.demo_project_manager]
}

resource "google_project_service" "demo_project_cloud_build" {
  project = var.demo_project_id
  service = "cloudbuild.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_iam_binding.role_service_usage_admin,google_project_service.demo_project_manager]
}

resource "google_project_service" "demo_project_service_networking" {
  project = var.demo_project_id
  service = "servicenetworking.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_iam_binding.role_service_usage_admin,google_project_service.demo_project_manager]
}

resource "google_project_service" "demo_project_cloud_sql" {
  project = var.demo_project_id
  service = "sqladmin.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_iam_binding.role_service_usage_admin,google_project_service.demo_project_manager]
}

resource "google_project_service" "demo_project_appengine" {
  project = var.demo_project_id
  service = "appengine.googleapis.com"
  disable_on_destroy = false

  depends_on = [google_project_iam_binding.role_service_usage_admin,google_project_service.demo_project_manager]
}