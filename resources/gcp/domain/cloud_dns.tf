// zone
resource "google_dns_managed_zone" "gcp_zone_demo_dashboard" {
  name        = "remorph-ml"
  dns_name    = "${var.gcp_domain_name}."
  labels      = {
    environment = var.env_id
  }

  depends_on = [var.gcp_demo_dashboard_api_dns]
}
/*
// point A to CloudRun
resource "google_dns_record_set" "demo_cloud_run_a_record" {
  name         = google_dns_managed_zone.gcp_zone_demo_dashboard.dns_name
  managed_zone = google_dns_managed_zone.gcp_zone_demo_dashboard.name
  type         = "A"
  ttl          = 300
  rrdatas      = [
    "216.239.32.21",
    "216.239.34.21",
    "216.239.36.21",
    "216.239.38.21"
  ]

  depends_on = [google_dns_managed_zone.gcp_zone_demo_dashboard]
}

resource "google_dns_record_set" "demo_cloud_run_aaaa_record" {
  name         = google_dns_managed_zone.gcp_zone_demo_dashboard.dns_name
  managed_zone = google_dns_managed_zone.gcp_zone_demo_dashboard.name
  type         = "AAAA"
  ttl          = 300
  rrdatas      = [
  "2001:4860:4802:32::15",
  "2001:4860:4802:34::15",
  "2001:4860:4802:36::15",
  "2001:4860:4802:38::15"
  ]

  depends_on = [google_dns_managed_zone.gcp_zone_demo_dashboard]
}
*/
// point A to LB
resource "google_dns_record_set" "demo_lb_a_record" {
  name         = google_dns_managed_zone.gcp_zone_demo_dashboard.dns_name
  managed_zone = google_dns_managed_zone.gcp_zone_demo_dashboard.name
  type         = "A"
  ttl          = 300
  rrdatas      = [
    var.demo_dashboard_lb_http_address
  ]

  depends_on = [google_dns_managed_zone.gcp_zone_demo_dashboard]
}