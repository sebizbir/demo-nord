output "gcp_demo_dashboard_name_servers" {
  value = google_dns_managed_zone.gcp_zone_demo_dashboard.name_servers
}

output "demo_lb_a_record_id" {
  value = google_dns_record_set.demo_lb_a_record.id
}