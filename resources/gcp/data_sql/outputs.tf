output "demo_dashboard_mysql_instance_connection_name" {
  value = google_sql_database_instance.demo_dashboard_mysql_instance.connection_name
}
output "demo_dashboard_mysql_instance_ip" {
  value = google_sql_database_instance.demo_dashboard_mysql_instance.ip_address
}
output "demo_dashboard_mysql_instance_name" {
  value = google_sql_database_instance.demo_dashboard_mysql_instance.name
}

// services
output "demo_dashboard_database_id" {
  value = google_sql_database.demo_dashboard_database.id
}

output "demo_dashboard_mysql_db_users_id" {
  value = google_sql_user.demo_dashboard_mysql_db_users.id
}

output "demo_dashboard_mysql_db_client_cert_id" {
  value = google_sql_ssl_cert.demo_dashboard_mysql_db_client_cert.id
}
