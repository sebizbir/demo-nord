#################################################################################
#                                 MySQL Database                                #
#################################################################################
########################################
#            Prefix for DB             #
########################################
resource "random_id" "demo_dashboard_mysql_db_name_suffix" {
  byte_length = 5

  depends_on = [var.api_cloud_sql,var.api_service_networking]
}
########################################
#              DB instance             #
########################################
resource "google_sql_database_instance" "demo_dashboard_mysql_instance" {
  name             = "${var.demo_dashboard_mysql_database_instance_name}-${random_id.demo_dashboard_mysql_db_name_suffix.hex}"
  database_version = var.demo_dashboard_mysql_database_version    # MYSQL_5_7

  region = var.demo_dashboard_mysql_database_region

  settings {
    tier = var.demo_dashboard_mysql_database_tier   #db-f1-micro
    ip_configuration {
      ipv4_enabled    = var.demo_dashboard_mysql_database_ipv4_enabled      #true/false
    }
  }

  depends_on = [var.api_cloud_sql,var.api_service_networking,random_id.demo_dashboard_mysql_db_name_suffix]
}
########################################
#               Database               #
########################################
resource "google_sql_database" "demo_dashboard_database" {
  name     = var.demo_dashboard_mysql_database_name
  instance = google_sql_database_instance.demo_dashboard_mysql_instance.name

  depends_on = [var.api_cloud_sql,var.api_service_networking,google_sql_database_instance.demo_dashboard_mysql_instance]
}
########################################
#                DB users              #
########################################
resource "google_sql_user" "demo_dashboard_mysql_db_users" {
  name     = var.demo_dashboard_mysql_sql_user_name
  instance = google_sql_database_instance.demo_dashboard_mysql_instance.name
  password = var.demo_dashboard_mysql_sql_user_password

  depends_on = [var.api_cloud_sql,var.api_service_networking,google_sql_database_instance.demo_dashboard_mysql_instance]
}
########################################
#            DB SSL cert               #
########################################
resource "google_sql_ssl_cert" "demo_dashboard_mysql_db_client_cert" {
  common_name = "demo-dashboard-db-client-cert"
  instance    = google_sql_database_instance.demo_dashboard_mysql_instance.name

  depends_on = [var.api_cloud_sql,var.api_service_networking,google_sql_database_instance.demo_dashboard_mysql_instance]
}

