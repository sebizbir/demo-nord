
output "demo_dashboard_lb_http_address" {
  value = google_compute_address.regional_lb_http_address.address
}

output "demo_dashboard_fwd_id" {
  value = google_compute_forwarding_rule.demo_dashboard_fwd.id
}

output "demo_dashboard_tp_id" {
  value = google_compute_target_pool.demo_dashboard_tp.id
}