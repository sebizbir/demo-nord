####################################
# CREATE ADDRESS
####################################

resource "google_compute_address" "regional_lb_http_address" {
  name = "glb-static-address"
  address_type = "EXTERNAL"
  region = var.demo_project_region
  project = var.demo_project_id
  network_tier = "STANDARD"

  depends_on = [var.demo_dashboard_instance_group_manager_id]
}


####################################
# CREATE FORWARDING RULE
####################################

resource "google_compute_forwarding_rule" "demo_dashboard_fwd" {
  provider              = google-beta
  project               = var.demo_project_id
  name                  = "demo-dashboard-fwd"
  target                = google_compute_target_pool.demo_dashboard_tp.self_link
  load_balancing_scheme = "EXTERNAL"
  port_range            = "5000"
  ip_address            = google_compute_address.regional_lb_http_address.address
  ip_protocol           = "TCP"
  network_tier = "STANDARD"

  depends_on = [var.demo_dashboard_instance_group_manager_id, google_compute_address.regional_lb_http_address, google_compute_target_pool.demo_dashboard_tp]
}

###################################
# CREATE TARGET POOL
###################################

data "google_compute_instance_group" "demo_dashboard_instance_group" {
  name = var.demo_dashboard_instance_group_manager_name
  zone = var.demo_dashboard_instance_group_manager_zone

  depends_on = [var.demo_dashboard_instance_group_manager_id]
}

resource "google_compute_target_pool" "demo_dashboard_tp" {
  provider         = google-beta
  project          = var.demo_project_id
  name             = "demo-dashboard-tp"
  region           = var.demo_project_region

  instances = data.google_compute_instance_group.demo_dashboard_instance_group.instances

  depends_on = [var.demo_dashboard_instance_group_manager_id]
}
