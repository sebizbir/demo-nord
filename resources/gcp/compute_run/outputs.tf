// services
output "cloud_run_demo_dashboard_main_id" {
  value = google_cloud_run_service.cloud_run_demo_dashboard_main.id
}

output "cloud_run_demo_dashboard_domain_mapping_id" {
  value = google_cloud_run_domain_mapping.cloud_run_demo_dashboard_domain_mapping.id
}

output "cloud_run_demo_dashboard_noauth_id" {
  value = google_cloud_run_service_iam_policy.cloud_run_demo_dashboard_noauth.id
}