#################################################################################
#                                 Cloud Run instances                           #
#################################################################################
########################################
#                Service               #
########################################
resource "google_cloud_run_service" "cloud_run_demo_dashboard_main" {
  provider = "google-beta"
  name     = var.cloud_run_demo_dashboard_service_name
  location = var.cloud_run_demo_dashboard_service_location

  metadata {
    namespace = var.cloud_run_demo_dashboard_service_namespace
    annotations = {
      "autoscaling.knative.dev/maxScale"      = "1"
      "run.googleapis.com/cloudsql-instances" = "${var.demo_project_id}:${var.demo_dashboard_mysql_database_region}:${var.demo_dashboard_mysql_instance_name}"
    }
  }

  spec {
    containers {
      image = var.cloud_run_demo_dashboard_service_image
      env {
        name = "DB_HOST"
        value = "127.0.0.1"
      }
      env {
        name = "DB_NAME"
        value = "${var.demo_dashboard_mysql_database_name}"
      }
      env {
        name = "DB_USER"
        value = "${var.demo_dashboard_mysql_sql_user_name}"
      }
      env {
        name = "DB_PASSWORD"
        value = "${var.demo_dashboard_mysql_sql_user_password}"
      }
      env {
        name = "CLOUDSQL_INSTANCE"
        value = "${var.demo_project_id}:${var.demo_dashboard_mysql_database_region}:${var.demo_dashboard_mysql_instance_name}"
      }
    }
  }

  depends_on = [var.demo_dashboard_mysql_instance_name]
}

resource "google_cloud_run_domain_mapping" "cloud_run_demo_dashboard_domain_mapping" {
  location = var.cloud_run_demo_dashboard_service_location
  name     = var.gcp_domain_name

  metadata {
    namespace = var.cloud_run_demo_dashboard_service_namespace
  }

  spec {
    route_name = google_cloud_run_service.cloud_run_demo_dashboard_main.name
    force_override = true
  }

  depends_on = [var.api_cloud_sql,google_cloud_run_service.cloud_run_demo_dashboard_main]
}

########################################
#             Service IAM              #
########################################
data "google_iam_policy" "cloud_run_demo_dashboard_noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "cloud_run_demo_dashboard_noauth" {
  location    = google_cloud_run_service.cloud_run_demo_dashboard_main.location
  project     = var.demo_project_id
  service     = google_cloud_run_service.cloud_run_demo_dashboard_main.name

  policy_data = data.google_iam_policy.cloud_run_demo_dashboard_noauth.policy_data

  depends_on = [data.google_iam_policy.cloud_run_demo_dashboard_noauth,google_cloud_run_service.cloud_run_demo_dashboard_main]
}
