# Project
variable "demo_project_id" {}
variable "gcp_domain_name" {}
# Cloud Run
variable "cloud_run_demo_dashboard_service_name" {}
variable "cloud_run_demo_dashboard_service_location" {}
variable "cloud_run_demo_dashboard_service_namespace" {}
variable "cloud_run_demo_dashboard_service_image" {}
# SQL
variable "api_cloud_sql" {}
variable "demo_dashboard_mysql_database_region" {}
variable "demo_dashboard_mysql_instance_name" {}
variable "demo_dashboard_mysql_database_name" {}
variable "demo_dashboard_mysql_sql_user_name" {}
variable "demo_dashboard_mysql_sql_user_password" {}