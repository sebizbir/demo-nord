/*
App Engine applications cannot be deleted once they're created;
you have to delete the entire project to delete the application.
Terraform will report the application has been successfully deleted;
this is a limitation of Terraform, and will go away in the future.
Terraform is not able to delete App Engine applications.
*/

data "google_project" "demo_project" {
  project_id = "soy-antenna-264311"
}

resource "google_app_engine_application" "app_engine_app" {
  project     = data.google_project.demo_project.project_id
  location_id = var.demo_project_region
}

resource "google_app_engine_standard_app_version" "app_engine_version" {
  version_id = "app-engine-v1"
  service    = "default"
  runtime    = "php72"

  entrypoint {
    shell = "shell run app"
  }

  deployment {
    zip {
      source_url = "https://storage.googleapis.com/${var.app_engine_code_bucket_name}/${var.app_engine_code_bucket_object_1_name}"
    }
  }

  delete_service_on_destroy = true

  depends_on = [google_app_engine_application.app_engine_app,var.role_appengine_admin_id,var.api_appengine,var.demo_dashboard_mysql_instance_name]
}