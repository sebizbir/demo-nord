# Project
variable "api_appengine" {}
variable "demo_project_id" {}
variable "demo_project_region" {}
# SQL
variable "api_cloud_sql" {}
variable "role_appengine_admin_id" {}
variable "demo_dashboard_mysql_instance_name" {}
variable "demo_dashboard_mysql_database_region" {}
variable "demo_dashboard_mysql_database_name" {}
variable "demo_dashboard_mysql_sql_user_name" {}
variable "demo_dashboard_mysql_sql_user_password" {}
# Bucket
variable "app_engine_code_bucket_name" {}
variable "app_engine_code_bucket_object_1_name" {}