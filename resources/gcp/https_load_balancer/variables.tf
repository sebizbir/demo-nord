variable "enable_ssl" {}
variable "gcp_domain_name" {}

variable "network_name" {}
variable "demo_project_id" {}
variable "demo_project_region" {}

variable "demo_dashboard_instance_group_manager_zone" {}
variable "demo_dashboard_instance_group_manager_name" {}
variable "demo_dashboard_instance_group_manager_id" {}
variable "demo_dashboard_instance_group_manager_full" {}