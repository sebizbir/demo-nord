
output "demo_dashboard_lb_http_address" {
  value = google_compute_address.regional_lb_http_address.address
}

output "demo_dashboard_http_forwarding_rule_id" {
  value = google_compute_forwarding_rule.demo_dashboard_http_forwarding_rule.id
}