####################################
# CREATE ADDRESS
####################################

resource "google_compute_address" "regional_lb_http_address" {
  name = "glb-static-address"
  address_type = "EXTERNAL"
  region = var.demo_project_region
  project = var.demo_project_id
  network_tier = "STANDARD"

  depends_on = [var.demo_dashboard_instance_group_manager_id]
}
resource "google_compute_forwarding_rule" "demo_dashboard_http_forwarding_rule" {
  project    = var.demo_project_id
  name       = "demo-dashboard-http"
  target     = google_compute_target_http_proxy.demo_dashboard_http_proxy.self_link
  port_range = "80"
  network_tier = "STANDARD"
  ip_address = google_compute_address.regional_lb_http_address.address

  depends_on = [var.demo_dashboard_instance_group_manager_id,google_compute_target_http_proxy.demo_dashboard_http_proxy, google_compute_address.regional_lb_http_address]
}


resource "google_compute_target_http_proxy" "demo_dashboard_http_proxy" {
  project = var.demo_project_id
  name    = "demo-dashboard-http-proxy"
  url_map = google_compute_url_map.demo_dashboard_url_map.name

  depends_on = [var.demo_dashboard_instance_group_manager_id]
}


resource "google_compute_url_map" "demo_dashboard_url_map" {
  name        = "demo-dashboard-map"
  default_service = google_compute_backend_service.backend_service.self_link

  depends_on = [var.demo_dashboard_instance_group_manager_id]
}

resource "google_compute_backend_service" "backend_service" {
  name          = "backend-service"
  protocol = "HTTP"
  health_checks = [google_compute_http_health_check.health_check.self_link]
  backend {
    group = var.demo_dashboard_instance_group_manager_full
  }

  depends_on = [var.demo_dashboard_instance_group_manager_id]
}

resource "google_compute_http_health_check" "health_check" {
  name         = "monitor-backend-service"
  request_path = "/"

  timeout_sec        = 5
  check_interval_sec = 5
  port               = 80

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [var.demo_dashboard_instance_group_manager_id]
}
