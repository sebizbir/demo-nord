#################################################################################
#                                      Storage                                  #
#################################################################################
########################################
#               Buckets                #
########################################
resource "random_id" "demo_dashboard_bucket_name_suffix" {
  byte_length = 5
}
// bucket for app engine code
resource "google_storage_bucket" "app_engine_code_bucket" {
  name     = "${var.app_engine_code_bucket_name}-${random_id.demo_dashboard_bucket_name_suffix.hex}"
  location = var.app_engine_code_bucket_location
  storage_class = var.app_engine_code_bucket_class
  force_destroy = true
}

// archive code
data "archive_file" "app_engine_zip" {
  type        = "zip"
  source_dir = "../../resources/gcp/storage/flask"
  output_path = "../../resources/gcp/storage/flask.zip"
}

resource "google_storage_bucket_object" "app_engine_code_bucket_object_1" {
  name   = "flask.zip"
  source = "../../resources/gcp/storage/flask.zip"
  bucket = google_storage_bucket.app_engine_code_bucket.name

  depends_on = [data.archive_file.app_engine_zip, google_storage_bucket.app_engine_code_bucket]
}

