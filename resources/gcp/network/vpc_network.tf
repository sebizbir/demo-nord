#################################################################################
#                                   Network                                     #
#################################################################################
########################################
#            VPC network               #
########################################
resource "google_compute_network" "demo_dashboard" {
  provider                = "google"
  name                    = var.network_name
  auto_create_subnetworks = var.vpc_subnetworks

  depends_on = [var.gcp_demo_dashboard_api_compute]
}

