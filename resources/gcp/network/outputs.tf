output "demo_dashboard_network_id" {
  value = google_compute_network.demo_dashboard.id
}
output "demo_dashboard_network_name" {
  value = google_compute_network.demo_dashboard.name
}

