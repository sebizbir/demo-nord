# VPC
output "vpc_id" {
  value = aws_vpc.vpc_id.id
}
# Subnets
output "public_subnet_cidr_az1" {
  value = aws_subnet.public_subnet_cidr_az1.id
}
output "public_subnet_cidr_az2" {
  value = aws_subnet.public_subnet_cidr_az2.id
}

output "private_subnet_cidr_az1_internal_data" {
  value = aws_subnet.private_subnet_cidr_az1_internal_data.id
}
output "private_subnet_cidr_az2_internal_data" {
  value = aws_subnet.private_subnet_cidr_az2_internal_data.id
}

