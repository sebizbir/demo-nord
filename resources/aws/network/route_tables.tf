##################################################################
#                   Public Subnets - Route table                 #
##################################################################
resource "aws_route_table" "public_route" {
  vpc_id 					= aws_vpc.vpc_id.id

  route {
    cidr_block 			= "0.0.0.0/0"
    gateway_id 			= aws_internet_gateway.igw.id
  }

  tags 					= {
    Name 				= "${var.env_id} - Public Routes"
  }

  depends_on = [aws_vpc.vpc_id,aws_internet_gateway.igw]
}

##################################################################
#                  Private Subnets - Route table                 #
##################################################################
resource "aws_route_table" "private_route" {
  vpc_id 					= aws_vpc.vpc_id.id

  route {
     cidr_block 			= "0.0.0.0/0"
     nat_gateway_id 		= aws_nat_gateway.nat_gw.id
  }

  tags 					= {
    Name 				= "${var.env_id} - Private Routes"
  }

  depends_on = [aws_vpc.vpc_id,aws_nat_gateway.nat_gw]
}