##########################################################################
#                  Public Subnet - External Subnet - AZ1                 #
##########################################################################
resource "aws_subnet" "public_subnet_cidr_az1" {
  vpc_id 					= aws_vpc.vpc_id.id

  cidr_block 				= var.public_subnet_cidr_az1
  availability_zone 		= "${var.aws_region}a"
  map_public_ip_on_launch   = true

  tags 					= {
    Name 				= "External Subnet - AZ1"
  }

  depends_on            = [aws_vpc.vpc_id]
}

resource "aws_route_table_association" "public_subnet_cidr_az1" {
  subnet_id 				= aws_subnet.public_subnet_cidr_az1.id
  route_table_id 			= aws_route_table.public_route.id

  depends_on                = [aws_subnet.public_subnet_cidr_az1,aws_route_table.public_route]
}
##########################################################################
#                  Public Subnet - External Subnet - AZ2                 #
##########################################################################
resource "aws_subnet" "public_subnet_cidr_az2" {
  vpc_id 					= aws_vpc.vpc_id.id

  cidr_block 				= var.public_subnet_cidr_az2
  availability_zone 		= "${var.aws_region}b"
  map_public_ip_on_launch   = true

  tags 					= {
    Name 				= "External Subnet - AZ2"
  }

  depends_on            = [aws_vpc.vpc_id]
}

resource "aws_route_table_association" "public_subnet_cidr_az2" {
  subnet_id 				= aws_subnet.public_subnet_cidr_az2.id
  route_table_id 			= aws_route_table.public_route.id

  depends_on                = [aws_subnet.public_subnet_cidr_az2,aws_route_table.public_route]
}

###################################################################################
#                  Private Subnet - Internal Data Subnet - AZ1                    #
###################################################################################
resource "aws_subnet" "private_subnet_cidr_az1_internal_data" {
  vpc_id 					= aws_vpc.vpc_id.id

  cidr_block 				= var.private_subnet_cidr_az1_internal_data
  availability_zone 		= "${var.aws_region}a"

  tags 					= {
    Name 				= "Internal Data Subnet - AZ1"
  }

  depends_on            = [aws_vpc.vpc_id]
}

resource "aws_route_table_association" "private_subnet_cidr_az1_internal_data" {
  subnet_id 				= aws_subnet.private_subnet_cidr_az1_internal_data.id
  route_table_id 			= aws_route_table.private_route.id

  depends_on                = [aws_subnet.private_subnet_cidr_az1_internal_data,aws_route_table.private_route]
}

###################################################################################
#                  Private Subnet - Internal Data Subnet - AZ2                    #
###################################################################################
resource "aws_subnet" "private_subnet_cidr_az2_internal_data" {
  vpc_id 					= aws_vpc.vpc_id.id

  cidr_block 				= var.private_subnet_cidr_az2_internal_data
  availability_zone 		= "${var.aws_region}b"

  tags 					= {
    Name 				= "Internal Data Subnet - AZ2"
  }

  depends_on = [aws_vpc.vpc_id]
}

resource "aws_route_table_association" "private_subnet_cidr_az2_internal_data" {
  subnet_id 				= aws_subnet.private_subnet_cidr_az2_internal_data.id
  route_table_id 			= aws_route_table.private_route.id

  depends_on                = [aws_subnet.private_subnet_cidr_az2_internal_data,aws_route_table.private_route]
}
