# VPC
variable "env_id" {}
variable "aws_region" {}
variable "vpc_dns" {}
variable "vpc_cidr" {}
# Subnets
variable "public_subnet_cidr_az1" {}
variable "public_subnet_cidr_az2" {}
variable "private_subnet_cidr_az1_internal_data" {}
variable "private_subnet_cidr_az2_internal_data" {}