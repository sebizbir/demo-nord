output "statefile_bucket_id" {
  value = aws_s3_bucket.statefile_bucket.id
}
output "static_index_file_id" {
  value = aws_s3_bucket_object.static_index_file.id
}

output "s3_uri" {
  value = aws_s3_bucket.statefile_bucket.bucket_domain_name
}