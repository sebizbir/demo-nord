#############################
#      Static S3 website    #
#############################
resource "aws_s3_bucket" "statefile_bucket" {
  region        = var.aws_region
  bucket        = var.s3_static_website_name
  force_destroy = true

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "PublicReadGetObject",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::${var.s3_static_website_name}/*"
    }
  ]
}
EOF
}
#############################
#   Files for S3 website    #
#############################
data "archive_file" "app_engine_zip" {
  type        = "zip"
  source_dir = "../../resources/aws/storage/flask"
  output_path = "../../resources/aws/storage/flask.zip"
}

resource "aws_s3_bucket_object" "static_index_file" {
  bucket        = var.s3_static_website_name
  key           = "flask.zip"
  source        = "../../resources/aws/storage/flask.zip"

  depends_on    = [aws_s3_bucket.statefile_bucket]
}
