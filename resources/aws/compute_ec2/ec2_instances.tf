#################################################################################
#                             Application Instances                             #
#################################################################################
resource "aws_launch_configuration" "application_launch_configuration" {

  name_prefix 		      		= "ApplicationLaunchConfiguration"
  image_id 				        	= "${var.application_image_id}"
  instance_type 		        = "${var.application_instance_type}"
  key_name                  = "${var.application_key_name}"
  root_block_device {
    volume_type             = "gp2"
    volume_size             = 50
    delete_on_termination   = true
  }

  ebs_block_device {
    device_name = "/dev/sdg"
    volume_type = "gp2"
    volume_size = 100
    encrypted   = true
    delete_on_termination = false
  }
  security_groups 			= ["${var.sg_ec2_app}"]
  user_data = <<HEREDOC
  <bash>
  sudo apt-get -y install unzip && sudo apt-get -y install python-pip && sudo mkdir /flask && cd /flask && sudo wget "${var.s3_uri}/flask.zip . && sudo unzip flask.zip && sudo pip install -r requirements.txt && sudo python runserver.py
  </bash>
  HEREDOC
}

resource "aws_autoscaling_group" "application_ag" {
  name_prefix                		= "Application"
  max_size             		= 1
  min_size             		= 1
  launch_configuration 		= "${aws_launch_configuration.application_launch_configuration.name}"
  vpc_zone_identifier  		= ["${var.public_subnet_cidr_az1}", "${var.public_subnet_cidr_az2}"]
  tag {
    key                 	= "Name"
    value               	= "App"
    propagate_at_launch 	= true
  }
  tag {
    key                 	= "Environment"
    value               	= "${var.env_id}"
    propagate_at_launch 	= true
  }
}