variable "env_id" {}
variable "s3_uri" {}
variable "sg_ec2_app" {}

variable "application_image_id" {}
variable "application_instance_type" {}
variable "application_key_name" {}

variable "public_subnet_cidr_az1" {}
variable "public_subnet_cidr_az2" {}