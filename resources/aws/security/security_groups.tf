#########################################################################################################
#                                           Security groups                                             #
#########################################################################################################
##################################################################
#                       SG - ECS Load Balancer                   #
##################################################################
resource "aws_security_group" "sg_ecs_load_balancer" {
  name 				= var.sg_ecs_load_balancer_name
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  vpc_id 			= var.vpc_id

  tags 				= {
    Name 			= var.sg_ecs_load_balancer_name
    Description 	= "Used to secure Application Load Balancer"
  }

  depends_on        = [var.vpc_id]
}
resource "aws_security_group_rule" "sg_ecs_load_balancer_1" {
  description              = "Internet - HTTP - Application Load balancer"
  from_port                = 80
  protocol                 = "tcp"
  security_group_id        = aws_security_group.sg_ecs_load_balancer.id
  cidr_blocks              = ["0.0.0.0/0"]
  to_port                  = 80
  type                     = "ingress"

  depends_on               = [aws_security_group.sg_ecs_load_balancer]
}
resource "aws_security_group_rule" "sg_ecs_load_balancer_2" {
  description              = "Internet - HTTPS - Application Load balancer"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.sg_ecs_load_balancer.id
  cidr_blocks              = ["0.0.0.0/0"]
  to_port                  = 443
  type                     = "ingress"

  depends_on               = [aws_security_group.sg_ecs_load_balancer]
}
##################################################################
#                       SG - EC2 App instances                   #
##################################################################
resource "aws_security_group" "sg_ec2_app" {
  name 				= var.sg_app_instances_name

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  vpc_id 				= var.vpc_id

  tags 				= {
    Name 			= var.sg_app_instances_name
    Description 	= "Used to secure Application instances"
  }

  depends_on        = [var.vpc_id]
}

resource "aws_security_group_rule" "sg_ec2_app_1" {
  description              = "Application Load Balancer - HTTP - Application instances"
  from_port                = 80
  protocol                 = "tcp"
  security_group_id        = aws_security_group.sg_ec2_app.id
  source_security_group_id = aws_security_group.sg_ecs_load_balancer.id
  to_port                  = 80
  type                     = "ingress"

  depends_on               = [aws_security_group.sg_ec2_app,aws_security_group.sg_ecs_load_balancer]
}
resource "aws_security_group_rule" "sg_ec2_app_2" {
  description              = "Application Load Balancer - HTTPS - Application instances"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.sg_ec2_app.id
  source_security_group_id = aws_security_group.sg_ecs_load_balancer.id
  to_port                  = 443
  type                     = "ingress"

  depends_on               = [aws_security_group.sg_ec2_app,aws_security_group.sg_ecs_load_balancer]
}

##################################################################
#                       SG - MySQL RDS database                  #
##################################################################
resource "aws_security_group" "sg_rds_mysql_database" {
  name 				= var.sg_rds_mysql_database_name

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  vpc_id 			= var.vpc_id

  tags 				= {
    Name 			= var.sg_rds_mysql_database_name
    Description 	= "Used to secure the MySQL RDS Database"
  }

  depends_on        = [var.vpc_id]
}

resource "aws_security_group_rule" "sg_rds_mysql_database_1" {
  description              = "Application instances - MySQL port - MySQL database"
  from_port                = 3306
  protocol                 = "tcp"
  security_group_id        = aws_security_group.sg_rds_mysql_database.id
  source_security_group_id = aws_security_group.sg_ec2_app.id
  to_port                  = 3306
  type                     = "ingress"

  depends_on               = [aws_security_group.sg_rds_mysql_database,aws_security_group.sg_ec2_app]
}