output "sg_ecs_load_balancer" {
  value = aws_security_group.sg_ecs_load_balancer.id
}
output "sg_ecs_load_balancer_name" {
  value = aws_security_group.sg_ecs_load_balancer.name
}
output "sg_ec2_app" {
  value = aws_security_group.sg_ec2_app.id
}
output "sg_ec2_app_name" {
  value = aws_security_group.sg_ec2_app.name
}

output "sg_rds_mysql_database" {
  value = aws_security_group.sg_rds_mysql_database.id
}
output "sg_rds_mysql_database_name" {
  value = aws_security_group.sg_rds_mysql_database.name
}