#########################################################################
#                        ACM SSL certificate for domain                 #
#########################################################################
resource "aws_acm_certificate" "acm_aws_demo_dashboard" {
  validation_method = "DNS"
  domain_name       = var.domain_name

  subject_alternative_names = [
    var.domain_name,
    "www.${var.domain_name}",
    var.s3_static_website_name,
    "www.${var.s3_static_website_name}",
  //  "cdn.${var.domain_name}"
  ]

  tags          = {
    Domain      = var.domain_name
    Environment = var.env_id
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on    = [aws_route53_zone.aws_zone_demo_dashboard]
}
