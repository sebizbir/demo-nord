#########################################################################
#                            Public zone                                #
#########################################################################
resource "aws_route53_zone" "aws_zone_demo_dashboard" {
  name          = var.domain_name
  force_destroy = true
  tags          = {
    Name        = var.domain_name
    Environment = var.env_id
  }
}
#########################################################################
#                                Records                                #
#########################################################################
##################################
#               WWW              #
##################################
resource "aws_route53_record" "aws_record_demo_dashboard_www_1" {
  zone_id = aws_route53_zone.aws_zone_demo_dashboard.zone_id
  name    = "www.${var.domain_name}"
  type    = "CNAME"
  ttl     = "300"

  records = ["${var.domain_name}"]

  depends_on               = [var.aws_lb_dns_name,var.aws_lb_zone_id]
}
##################################
#               WWW              #
##################################
resource "aws_route53_record" "aws_record_demo_dashboard_www_2" {
  zone_id = aws_route53_zone.aws_zone_demo_dashboard.zone_id
  name    = "www.${var.s3_static_website_name}"
  type    = "CNAME"
  ttl     = "300"

  records = ["${var.s3_static_website_name}"]

  depends_on               = [var.aws_lb_dns_name,var.aws_lb_zone_id]
}

##################################
#           Load balancer        #
##################################
resource "aws_route53_record" "aws_record_demo_dashboard_load_balancer" {
  zone_id = aws_route53_zone.aws_zone_demo_dashboard.zone_id
  name    = var.domain_name
  type    = "A"

  alias {
    name                   = var.aws_lb_dns_name
    zone_id                = var.aws_lb_zone_id
    evaluate_target_health = true
  }

  depends_on               = [var.aws_lb_dns_name,var.aws_lb_zone_id]
}
