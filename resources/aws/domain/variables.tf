# ACM
variable "demo_dashboard_validate_acm" {}
variable "env_id" {}
variable "domain_name" {}
# Route53
variable "aws_lb_dns_name" {}
variable "aws_lb_zone_id" {}
