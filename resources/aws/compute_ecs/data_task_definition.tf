data "aws_ecr_image" "ecr_image_uri" {
  repository_name = "my/service"
  image_tag       = "latest"
}

data "template_file" "task_definition" {
  template = file("task-definitions/main.json")
}
