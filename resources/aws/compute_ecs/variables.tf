# ecs cluster
variable "ecs_cluster_name" {}
variable "ecs_enable_container_insights" {}
# ecs service
variable "sg_ec2_app" {}
variable "ecs_service_name_demo_dashboard" {}
variable "ecs_service_desired_count_demo_dashboard" {}
variable "iam_role_ecs_service" {}
# ecs task definitions
variable "ecs_task_definition_demo_dashboard" {}
variable "ecr_demo_dashboard_name" {}
variable "image_uri" {}
# vpc
variable "vpc_id" {}
variable "env_id" {}
variable "aws_region" {}
variable "public_subnet_cidr_az1" {}
variable "public_subnet_cidr_az2" {}
variable "private_subnet_cidr_az1_internal_data" {}
variable "private_subnet_cidr_az2_internal_data" {}
# alb
variable "ecs_load_balancer_demo_dashboard_name" {}
variable "sg_ecs_load_balancer" {}