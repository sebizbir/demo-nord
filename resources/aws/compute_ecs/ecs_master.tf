##################################################################
#                 Master ECS Clusters                            #
##################################################################
resource "aws_ecs_cluster" "main_ecs_cluster" {
  name                  = var.ecs_cluster_name
  setting 				= {
    name                = "containerInsights"
    value               = var.ecs_enable_container_insights
  }
  tags 					= {
    Name 				= var.ecs_cluster_name
    Environment         = var.env_id
  }
}

