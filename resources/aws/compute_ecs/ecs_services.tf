##################################################################
#                     Demo dashboard Service                     #
##################################################################
resource "aws_ecs_service" "demo_dashboard_main" {
  name            = var.ecs_service_name_demo_dashboard
  cluster         = aws_ecs_cluster.main_ecs_cluster.id
  task_definition = aws_ecs_task_definition.ecs_task_definition.arn
  desired_count   = var.ecs_service_desired_count_demo_dashboard
  iam_role        = var.iam_role_ecs_service

  load_balancer {
    target_group_arn = aws_lb_target_group.ecs_load_balancer_tg_demo_dashboard_main.arn
    container_name = "main"
    container_port = 8080
  }
  network_configuration {
    subnets = [var.private_subnet_cidr_az1_internal_app, var.private_subnet_cidr_az2_internal_app]
    security_groups = [var.sg_ec2_app]
    assign_public_ip = true
  }
  tags              = {
    Name 			= var.ecs_service_name_demo_dashboard
    Description 	= "Main service for ECS cluster"
    Cluster         = aws_ecs_cluster.main_ecs_cluster.name
  }

  depends_on        = [aws_ecs_cluster.main_ecs_cluster,var.sg_ec2_app,var.private_subnet_cidr_az1_internal_app, var.private_subnet_cidr_az2_internal_app]
}
