##################################################################
#                     ECS demo_dashboard ALB                            #
##################################################################
resource "aws_lb" "ecs_load_balancer_demo_dashboard" {
  name               = var.ecs_load_balancer_demo_dashboard_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.sg_ecs_load_balancer]
  subnets            = [var.public_subnet_cidr_az1, var.public_subnet_cidr_az2]
  tags          = {
    Name        = var.ecs_load_balancer_demo_dashboard_name
    Environment = var.env_id
  }

  depends_on = [var.sg_ecs_load_balancer,var.public_subnet_cidr_az1, var.public_subnet_cidr_az2]
}

resource "aws_lb_target_group" "ecs_load_balancer_tg_demo_dashboard_main" {
  name     = "ecs-main-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  depends_on = [var.vpc_id]
}

resource "aws_lb_listener" "ecs_demo_dashboard_http" {
  load_balancer_arn = aws_lb.ecs_load_balancer_demo_dashboard.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.ecs_load_balancer_tg_demo_dashboard_main.id
    type             = "forward"
  }

  depends_on        = [aws_lb.ecs_load_balancer_demo_dashboard,aws_lb_target_group.ecs_load_balancer_tg_demo_dashboard_main]
}
