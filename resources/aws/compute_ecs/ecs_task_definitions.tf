
resource "aws_ecs_task_definition" "ecs_task_definition" {
  family                    = "wordpress-main"
  requires_compatibilities  = ["EC2"]
  container_definitions     = "${data.template_file.task_definition.rendered}"
  memory                   = "256"


  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [eu-west-2a, eu-west-2b]"
  }
  tags 					= {
    Name 				= var.ecs_cluster_name
    Environment         = var.env_id
  }
}