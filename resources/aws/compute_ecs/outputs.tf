output "aws_lb_dns_name" {
  value = aws_lb.ecs_load_balancer_demo_dashboard.dns_name
}
output "aws_lb_zone_id" {
  value = aws_lb.ecs_load_balancer_demo_dashboard.zone_id
}
