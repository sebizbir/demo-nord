# Subnets
variable "private_subnet_cidr_az1_internal_data" {}
variable "private_subnet_cidr_az2_internal_data" {}
# Security Groups
variable "sg_rds_mysql_database" {}
# MySQL
variable "rds_mysql_family_name" {}
variable "rds_mysql_identifier" {}
variable "rds_mysql_allocated_storage" {}
variable "rds_mysql_backup_retention_period" {}
variable "rds_mysql_backup_window" {}
variable "rds_mysql_maintenance_window" {}
variable "rds_mysql_multi_az" {}
variable "rds_mysql_storage_encrypted" {}
variable "rds_mysql_publicly_accessible" {}
variable "rds_mysql_storage_type" {}
variable "rds_mysql_engine" {}
variable "rds_mysql_engine_version" {}
variable "rds_mysql_db_name" {}
variable "rds_mysql_instance_class" {}
variable "rds_mysql_username" {}
variable "rds_mysql_password" {}


