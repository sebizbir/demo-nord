#################################################################################
#                                MySQL RDS Database                             #
#################################################################################
########################################
#        Parameter group               #
########################################
resource "aws_db_parameter_group" "db_parameter_group_mysql" {
  name_prefix   			= var.rds_mysql_identifier
  family 					= var.rds_mysql_family_name
}
########################################
#           Subnet group               #
########################################
resource "aws_db_subnet_group" "db_subnet_group_mysql" {
  name_prefix       		= var.rds_mysql_identifier
  subnet_ids 				= [var.private_subnet_cidr_az1_internal_data, var.private_subnet_cidr_az2_internal_data]

  tags 						= {
    Name 					= var.rds_mysql_identifier
  }

  depends_on                = [var.private_subnet_cidr_az1_internal_data, var.private_subnet_cidr_az2_internal_data]
}
########################################
#           DB instance                #
########################################
resource "aws_db_instance" "mysql_db_instance" {
  allocated_storage    		    = var.rds_mysql_allocated_storage
  auto_minor_version_upgrade    = "true"
  backup_retention_period 	    = var.rds_mysql_backup_retention_period
  backup_window 				= var.rds_mysql_backup_window
  copy_tags_to_snapshot 		= true
  maintenance_window 			= var.rds_mysql_maintenance_window
  db_subnet_group_name 		    = aws_db_subnet_group.db_subnet_group_mysql.id
  skip_final_snapshot 		    = "true"
  multi_az 					    = var.rds_mysql_multi_az
  storage_encrypted 			= var.rds_mysql_storage_encrypted
  publicly_accessible 		    = var.rds_mysql_publicly_accessible
  storage_type         		    = var.rds_mysql_storage_type
  engine               		    = var.rds_mysql_engine
  engine_version       		    = var.rds_mysql_engine_version
  instance_class       		    = var.rds_mysql_instance_class
  name                 		    = var.rds_mysql_db_name
  username             		    = var.rds_mysql_username
  password             		    = var.rds_mysql_password
  parameter_group_name 		    = aws_db_parameter_group.db_parameter_group_mysql.id
  vpc_security_group_ids 		= [var.sg_rds_mysql_database]

  depends_on                    = [aws_db_subnet_group.db_subnet_group_mysql,var.sg_rds_mysql_database]
}
