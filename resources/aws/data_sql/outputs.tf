output "mysql_db_instance" {
  value = aws_db_instance.mysql_db_instance.id
}
output "mysql_db_instance_address" {
  value = aws_db_instance.mysql_db_instance.address
}
