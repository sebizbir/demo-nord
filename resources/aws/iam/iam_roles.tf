##################################################
#                Terraform user                  #
#      User should have IAMFullAccess policy     #
##################################################
#### user should have IAMFullAccess
data "aws_iam_user" "terraform_creator_user" {
  user_name = "terraform"
}
##################################################
#                Terraform group                 #
##################################################
resource "aws_iam_group" "terraform_creator_group" {
  name = "terraform"
}
##################################################
#             Assign user to a group             #
##################################################
resource "aws_iam_group_membership" "terraform_creator_group_membership" {
  name = "terraform-group-membership"
  users = [
    data.aws_iam_user.terraform_creator_user.user_name
  ]

  group = aws_iam_group.terraform_creator_group.name
}
##################################################
#           Assign policy to a group             #
##################################################
#########################
#    ELB Full Access    #
#########################
resource "aws_iam_group_policy_attachment" "policy_load_balancing_full_access" {
  group      = aws_iam_group.terraform_creator_group.name
  policy_arn = "arn:aws:iam::aws:policy/ElasticLoadBalancingFullAccess"
}
##########################
#    VPC Full Access     #
##########################
resource "aws_iam_group_policy_attachment" "policy_vpc_full_access" {
  group      = aws_iam_group.terraform_creator_group.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonVPCFullAccess"
}
##########################
#  Route53 Full Access   #
##########################
resource "aws_iam_group_policy_attachment" "policy_route53_full_access" {
  group      = aws_iam_group.terraform_creator_group.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonRoute53FullAccess"
}
##########################
#    ACM Full Access     #
##########################
resource "aws_iam_group_policy_attachment" "policy_acm_full_access" {
  group      = aws_iam_group.terraform_creator_group.name
  policy_arn = "arn:aws:iam::aws:policy/AWSCertificateManagerFullAccess"
}

##################################################
#                     ECS                        #
##################################################
resource "aws_iam_role" "ecs_service" {
  name = "ecsServiceRole"

  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "ecs_service" {
  name = "ecs_service_iam_policy"
  role = aws_iam_role.ecs_service.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:Describe*",
        "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
        "elasticloadbalancing:DeregisterTargets",
        "elasticloadbalancing:Describe*",
        "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
        "elasticloadbalancing:RegisterTargets"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}
