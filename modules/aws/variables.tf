#########################################################################
#                                DEFAULT                                #
#########################################################################
variable "aws_region" {
  description 	= "EC2 Region for the VPC"
  default 		= "eu-central-1"
}

variable "env_id" {
  description   = "Environment name"
  default       = "Demo-dashboard Environment"
}
#########################################################################
#                                NETWORK                                #
#########################################################################
#########################################################
#                         VPC                           #
#########################################################
variable "vpc_cidr" {
  description 	= "CIDR for the VPC"
  default 		= "10.63.0.0/22"
}
variable "vpc_dns" {
  description   = "Enable dns hostnames"
  default       = true
}
#########################################################
#                         Subnets                       #
#########################################################
################################################
#                   Public                     #
################################################
variable "public_subnet_cidr_az1" {
  description 	= "CIDR for the Public Subnet - External Subnet - AZ1"
  default 		= "10.63.1.0/26"
}
variable "public_subnet_cidr_az2" {
  description 	= "CIDR for the Public Subnet - External Subnet - AZ2"
  default 		= "10.63.1.64/26"
}
################################################
#                  Private                     #
################################################
variable "private_subnet_cidr_az1_internal_data" {
  description 	= "CIDR for the Private Data Subnet - Internal Subnet - AZ1"
  default 		= "10.63.3.0/26"
}
variable "private_subnet_cidr_az2_internal_data" {
  description 	= "CIDR for the Private Data Subnet - Internal Subnet - AZ2"
  default 		= "10.63.3.64/26"
}
#########################################################################
#                             SECURITY                                  #
#########################################################################
variable "sg_ecs_load_balancer_name" {
  description = "Security group name for Application Load balancer"
  default     = "sg_ecs_load_balancer"
}
variable "sg_app_instances_name" {
  description = "Security group name for Application instances"
  default     = "sg_app_instances_name"
}
variable "sg_rds_mysql_database_name" {
  description = "Security group name for RDS MySQL database"
  default     = "sg_rds_mysql_database"
}
#########################################################################
#                                DOMAIN                                 #
#########################################################################
#####################################
#              ACM/DNS              #
#####################################
variable "demo_dashboard_validate_acm" {
  description = "true/false value for enabling DNS validation in ACM/Route53"
  type = bool
  default = false
}

variable "domain_name" {
  description = "Domain name for a project and SSL certificate."
  default     = "domain.com"
}
#########################################################################
#                               STORAGE                                 #
#########################################################################
variable "s3_static_website_name" {
  description = "Name for a bucket, should be same as hosted zone for Route53: variable domain_name"
  default = "aws-demo-dashboard-app-engine-code"
}

#########################################################################
#                                COMPUTE                                #
#########################################################################
####################################################################
#                               EC2                                #
####################################################################
variable "application_image_id" {
  default = "ami-0ac9951fa8de08218"
}
variable "application_instance_type" {
  default = "t2.micro"
}
variable "application_key_name" {
  default = "demo"
}
####################################################################
#                               ECS                                #
#  https://www.terraform.io/docs/providers/aws/r/ecs_service.html  #
####################################################################
variable "ecs_cluster_name" {
  description = "Name for ECS cluster"
  default      = "demo_dashboard_ECS"
}
variable "ecs_enable_container_insights" {
  description = "Enabled/Disabled, container Insights for ECS cluster"
  default 	  = "Enabled"
}
variable "ecs_service_desired_count_demo_dashboard" {
  description = "Desired count of running tasks for ECS service"
  default     = 1
}
variable "ecs_service_name_demo_dashboard" {
  description = "Name for ECS service"
  default     = "demo_dashboard_service"
}
variable "ecs_task_definition_demo_dashboard" {
  description = "Name for ECS Task definition"
  default     = "demo_dashboard_task_definition"
}
variable "ecs_load_balancer_demo_dashboard_name" {
  description = "Name for ECS Application Load balancer"
  default     = "ecs-lb"
}

#########################################################################
#                             REPOSITORIES                              #
#########################################################################
variable "ecr_demo_dashboard_name" {
  description = "Name for ECR repository"
  default     = "ecr-demo-dashboard"
}
variable "ecr_demo_dashboard_mutability" {
  description = "ECR repository mutability"
  default     = "MUTABLE"
}
variable "image_uri" {
  default = ""
}
#########################################################################
#                               DATA - SQL                              #
#     https://www.terraform.io/docs/providers/aws/r/db_instance.html    #
#########################################################################
#####################################
#               MySQL               #
#####################################
variable "rds_mysql_identifier" {
  description = "Database identifier"
  default     = "terraform-demo"
}
variable "rds_mysql_allocated_storage" {
  description = "Allocated storage in GB"
  default     = 20
}
variable "rds_mysql_backup_retention_period" {
  description = "Retention period"
  default     = 7
}
variable "rds_mysql_backup_window" {
  description = "Backup window (should be >=30 mins)"
  default     = "01:37-02:07"
}
variable "rds_mysql_maintenance_window" {
  description = "Maintenance window (should be >=30 mins)"
  default     = "Mon:23:29-Mon:23:59"
}
variable "rds_mysql_multi_az" {
  description = "Enable/disable Multi AZ failover option"
  default     = true
}
variable "rds_mysql_storage_encrypted" {
  description = "Encrypt/Not encrypt underlying storage"
  default     = true
}
variable "rds_mysql_publicly_accessible" {
  description = "Enable public/private access"
  default     = false
}
variable "rds_mysql_storage_type" {
  description = "Underlying storage type"
  default     = "gp2"
}
variable "rds_mysql_engine" {
  description = "MySQL engine type"
  default     = "mysql"
}
variable "rds_mysql_engine_version" {
  description = "MySQL engine version"
  default     = "5.5.61"
}
variable "rds_mysql_instance_class" {
  description = "RDS instance class for the underlying machine"
  default     = "db.t2.micro"
}
variable "rds_mysql_db_name" {
  description = "MySQL database name"
  default     = "demo-dashboard"
}
variable "rds_mysql_username" {
  description = "Initial user"
  default     = "demo"
}
variable "rds_mysql_password" {
  description = "Initial password"
  default     = "R@nd0m"
}
variable "rds_mysql_family_name" {
  description = "MySQL database family name"
  default     = "mysql5.5"
}