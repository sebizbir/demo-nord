#########################################################################
#                     Network - VPC, CIDR's, Subnets                    #
#########################################################################
module "network" {
  source                                      = "../../resources/aws/network/"
  # VPC data
  aws_region                                  = var.aws_region
  env_id                                      = var.env_id
  vpc_dns                                     = var.vpc_dns
  vpc_cidr                                    = var.vpc_cidr
  # Subnets
  public_subnet_cidr_az1                      = var.public_subnet_cidr_az1
  public_subnet_cidr_az2                      = var.public_subnet_cidr_az2
  private_subnet_cidr_az1_internal_data       = var.private_subnet_cidr_az1_internal_data
  private_subnet_cidr_az2_internal_data       = var.private_subnet_cidr_az2_internal_data
}
#########################################################################
#                             IAM  - IAM roles                          #
#########################################################################
module "iam" {
  source                                      = "../../resources/aws/iam/"
}
#########################################################################
#                       Security - Security groups                      #
#########################################################################
// depends on network module
module "security" {
  source                                      = "../../resources/aws/security/"
  # VPC data
  vpc_id                                      = module.network.vpc_id
  # Security groups
  sg_ecs_load_balancer_name                   = var.sg_ecs_load_balancer_name
  sg_app_instances_name                       = var.sg_app_instances_name
  sg_rds_mysql_database_name                  = var.sg_rds_mysql_database_name
}
#########################################################################
#                        Repositories - ECR                             #
#########################################################################
module "repositories" {
  source                                      = "../../resources/aws/repositories/"
  # VPC data
  env_id                                      = var.env_id
  # ECR data
  ecr_demo_dashboard_name                            = var.ecr_demo_dashboard_name
  ecr_demo_dashboard_mutability                      = var.ecr_demo_dashboard_mutability
}
#########################################################################
#                         Storage - S3 buckets                          #
#########################################################################
module "storage" {
  source                                      = "../../resources/aws/storage/"
  # VPC data
  aws_region                                  = var.aws_region
  # S3 data
  s3_static_website_name                      = var.s3_static_website_name
}
#########################################################################
#                      Data - Databases (MSSQL, MySQL)                  #
#########################################################################
// depends on network, security modules
module "data_sql" {
  source                                      = "../../resources/aws/data_sql/"
  # Subnets
  private_subnet_cidr_az1_internal_data       = module.network.private_subnet_cidr_az1_internal_data
  private_subnet_cidr_az2_internal_data       = module.network.private_subnet_cidr_az2_internal_data
  # Security groups
  sg_rds_mysql_database                       = module.security.sg_rds_mysql_database
  # Mysql RDS data
  rds_mysql_db_name                           = var.rds_mysql_db_name
  rds_mysql_family_name                       = var.rds_mysql_family_name
  rds_mysql_identifier                        = var.rds_mysql_identifier
  rds_mysql_allocated_storage                 = var.rds_mysql_allocated_storage
  rds_mysql_backup_retention_period           = var.rds_mysql_backup_retention_period
  rds_mysql_backup_window                     = var.rds_mysql_backup_window
  rds_mysql_maintenance_window                = var.rds_mysql_maintenance_window
  rds_mysql_multi_az                          = var.rds_mysql_multi_az
  rds_mysql_storage_encrypted                 = var.rds_mysql_storage_encrypted
  rds_mysql_publicly_accessible               = var.rds_mysql_publicly_accessible
  rds_mysql_storage_type                      = var.rds_mysql_storage_type
  rds_mysql_engine                            = var.rds_mysql_engine
  rds_mysql_engine_version                    = var.rds_mysql_engine_version
  rds_mysql_instance_class                    = var.rds_mysql_instance_class
  rds_mysql_username                          = var.rds_mysql_username
  rds_mysql_password                          = var.rds_mysql_password
}
#########################################################################
#                         Compute Beanstalk                             #
#########################################################################
module "compute_beanstalk" {
  source                                      = "../../resources/aws/compute_beanstalk/"
  statefile_bucket_id                         = module.storage.statefile_bucket_id
  static_index_file_id                        = module.storage.static_index_file_id
}
#########################################################################
#                         Compute EC2                                   #
#########################################################################
module "compute_ec2" {
  source                                      = "../../resources/aws/compute_ec2/"
  s3_uri                                      = module.storage.s3_uri
  application_image_id                        = var.application_image_id
  application_instance_type                   = var.application_instance_type
  application_key_name                        = var.application_key_name
  sg_ec2_app                                  = module.security.sg_ec2_app
  env_id                                      = var.env_id
  public_subnet_cidr_az1                      = module.network.public_subnet_cidr_az1
  public_subnet_cidr_az2                      = module.network.public_subnet_cidr_az2
}
#########################################################################
#     Compute ECS - ECS Cluster, Services, Tasks and Load balancer      #
#########################################################################
// depends on network, iam, security, storage, functions modules
module "compute_ecs" {
  source                                      = "../../resources/aws/compute_ecs/"
  # VPC data
  env_id                                      = var.env_id
  vpc_id                                      = module.network.vpc_id
  aws_region                                  = var.aws_region
  # IAM
  iam_role_ecs_service                        = module.iam.iam_role_ecs_service
  # ECS data
  ecs_cluster_name                            = var.ecs_cluster_name
  ecs_enable_container_insights               = var.ecs_enable_container_insights
  ecs_service_desired_count_demo_dashboard    = var.ecs_service_desired_count_demo_dashboard
  ecs_service_name_demo_dashboard             = var.ecs_service_name_demo_dashboard
  ecs_task_definition_demo_dashboard          = var.ecs_task_definition_demo_dashboard
  ecr_demo_dashboard_name                     = module.repositories.ecr_demo_dashboard_name
  image_uri                                   = var.image_uri
  # LB
  ecs_load_balancer_demo_dashboard_name              = var.ecs_load_balancer_demo_dashboard_name
  s3_static_website_name                      = var.s3_static_website_name
  # Security
  sg_ec2_app                                  = module.security.sg_ec2_app
  sg_ecs_load_balancer                        = module.security.sg_ecs_load_balancer
  # Subnets
  public_subnet_cidr_az1                      = module.network.public_subnet_cidr_az1
  public_subnet_cidr_az2                      = module.network.public_subnet_cidr_az2
  private_subnet_cidr_az1_internal_data       = module.network.private_subnet_cidr_az1_internal_data
  private_subnet_cidr_az2_internal_data       = module.network.private_subnet_cidr_az2_internal_data
}
#########################################################################
#                        SSL - ACM and Route53                          #
#########################################################################
// depends on compute, storage, cdn modules
module "domain" {
  source                                      = "../../resources/aws/domain/"
  demo_dashboard_validate_acm                 = var.demo_dashboard_validate_acm
  # VPC data
  env_id                                      = var.env_id
  # ACM data
  domain_name                                 = var.domain_name
  # Route53 data
  aws_lb_dns_name                             = module.compute_ecs.aws_lb_dns_name
  aws_lb_zone_id                              = module.compute_ecs.aws_lb_zone_id
  # S3 data
  s3_static_website_name                      = var.s3_static_website_name
}

