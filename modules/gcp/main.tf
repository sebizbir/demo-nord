#########################################################################
#                     IAM - Users and service accounts                  #
#########################################################################
module "iam_api" {
  source                                  = "../../resources/gcp/iam_api/"
  #
  # Project data
  demo_project_id                         = var.demo_project_id
  demo_project_number                     = var.demo_project_number
}

#########################################################################
#                     Network - VPC, CIDR's, Subnets                    #
#########################################################################
module "network" {
  source                                  = "../../resources/gcp/network/"
  # VPC data
  gcp_demo_dashboard_api_compute          = module.iam_api.api_compute
  network_name                            = var.network_name
  vpc_subnetworks                         = var.vpc_subnetworks
}
#########################################################################
#                        Network Load Balancer                          #
#########################################################################
module "network_load_balancer" {
  source                                      = "../../resources/gcp/network_load_balancer/"
  network_name                                = var.network_name
  demo_project_id                             = var.demo_project_id
  demo_project_region                         = var.demo_project_region
  demo_dashboard_instance_group_manager_zone  = module.compute_instances.demo_dashboard_instance_group_manager_zone
  demo_dashboard_instance_group_manager_name  = module.compute_instances.demo_dashboard_instance_group_manager_name
  demo_dashboard_instance_group_manager_id     = module.compute_instances.demo_dashboard_instance_group_manager_id
}

#########################################################################
#                       HTTP/HTTPS Load Balancer                        #
#########################################################################
module "https_load_balancer" {
  source                                      = "../../resources/gcp/https_load_balancer/"
  enable_ssl                                  = var.enable_ssl
  gcp_domain_name                             = var.gcp_domain_name
  network_name                                = var.network_name
  demo_project_id                             = var.demo_project_id
  demo_project_region                         = var.demo_project_region
  demo_dashboard_instance_group_manager_zone  = module.compute_instances.demo_dashboard_instance_group_manager_zone
  demo_dashboard_instance_group_manager_name  = module.compute_instances.demo_dashboard_instance_group_manager_name
  demo_dashboard_instance_group_manager_id    = module.compute_instances.demo_dashboard_instance_group_manager_id
  demo_dashboard_instance_group_manager_full  = module.compute_instances.demo_dashboard_instance_group_manager_full
}

#########################################################################
#                       Security - Firewall rules                       #
#########################################################################
module "security" {
  source                                          = "../../resources/gcp/security/"
  # VPC data
  network_name                                    = var.network_name
  demo_dashboard_network_id                       = module.network.demo_dashboard_network_id
  # FW - http/https
  demo_dashboard_http_https_allow_protocol        = var.demo_dashboard_http_https_allow_protocol
  demo_dashboard_http_https_allow_protocol_ports  = var.demo_dashboard_http_https_allow_protocol_ports
  demo_dashboard_http_https_source_ranges         = var.demo_dashboard_http_https_source_ranges
  demo_dashboard_http_https_target_tags           = var.demo_dashboard_http_https_target_tags
  # FW - ssh
  demo_dashboard_ssh_allow_protocol               = var.demo_dashboard_ssh_allow_protocol
  demo_dashboard_ssh_allow_protocol_ports         = var.demo_dashboard_ssh_allow_protocol_ports
  demo_dashboard_ssh_source_ranges                = var.demo_dashboard_ssh_source_ranges
  demo_dashboard_ssh_target_tags                  = var.demo_dashboard_ssh_target_tags
  # FW - app
  demo_dashboard_app_allow_protocol               = var.demo_dashboard_app_allow_protocol
  demo_dashboard_app_allow_protocol_ports         = var.demo_dashboard_app_allow_protocol_ports
  demo_dashboard_app_source_ranges                = var.demo_dashboard_app_source_ranges
  demo_dashboard_app_target_tags                  = var.demo_dashboard_app_target_tags
}

#########################################################################
#                         Storage - Storage buckets                     #
#########################################################################
module "storage" {
  source                                  = "../../resources/gcp/storage/"
  # Storage buckets

  app_engine_code_bucket_name             = var.app_engine_code_bucket_name
  app_engine_code_bucket_location         = var.app_engine_code_bucket_location
  app_engine_code_bucket_class            = var.app_engine_code_bucket_class
}

#########################################################################
#                            Domain - Cloud DNS                         #
#########################################################################
module "domain" {
  source                                  = "../../resources/gcp/domain/"
  gcp_demo_dashboard_api_dns              = module.iam_api.api_dns
  demo_dashboard_lb_http_address          = module.network_load_balancer.demo_dashboard_lb_http_address
  # VPC data
  env_id                                  = var.env_id
  gcp_domain_name                         = var.gcp_domain_name
}
/*
#########################################################################
#                           Compute - Cloud run                         #
#########################################################################
module "compute_run" {
  source                                  = "../../resources/gcp/compute_run/"
  # VPC data
  demo_project_id                                        = var.demo_project_id
  gcp_domain_name                                        = var.gcp_domain_name
  # CloudRun data
  cloud_run_demo_dashboard_service_name                  = var.cloud_run_demo_dashboard_service_name
  cloud_run_demo_dashboard_service_location              = var.cloud_run_demo_dashboard_service_location
  cloud_run_demo_dashboard_service_namespace             = var.cloud_run_demo_dashboard_service_namespace
  cloud_run_demo_dashboard_service_image                 = var.cloud_run_demo_dashboard_service_image
  # SQL
  api_cloud_sql                                         = module.iam_api.api_cloud_sql
  demo_dashboard_mysql_database_region                  = var.demo_dashboard_mysql_database_region
  demo_dashboard_mysql_instance_name                    = module.data_sql.demo_dashboard_mysql_instance_name
  demo_dashboard_mysql_database_name                    = var.demo_dashboard_mysql_database_name
  demo_dashboard_mysql_sql_user_name                    = var.demo_dashboard_mysql_sql_user_name
  demo_dashboard_mysql_sql_user_password                = var.demo_dashboard_mysql_sql_user_password
}
#########################################################################
#                           Compute - App Engine                        #
#########################################################################
module "compute_app_engine" {
  source                                  = "../../resources/gcp/compute_app_engine/"
  # App engine app
  role_appengine_admin_id = module.iam_api.role_appengine_admin_id
  api_appengine                                         = module.iam_api.api_appengine
  demo_project_id                                        = var.demo_project_id
  demo_project_region                                   = var.demo_project_region
  # App engine version
  # Bucket
  app_engine_code_bucket_name                           = module.storage.app_engine_code_bucket_name
  app_engine_code_bucket_object_1_name                       = module.storage.app_engine_code_bucket_object_1_name
  # SQL
  api_cloud_sql                                         = module.iam_api.api_cloud_sql
  demo_dashboard_mysql_database_region                  = var.demo_dashboard_mysql_database_region
  demo_dashboard_mysql_instance_name                    = module.data_sql.demo_dashboard_mysql_instance_name
  demo_dashboard_mysql_database_name                    = var.demo_dashboard_mysql_database_name
  demo_dashboard_mysql_sql_user_name                    = var.demo_dashboard_mysql_sql_user_name
  demo_dashboard_mysql_sql_user_password                = var.demo_dashboard_mysql_sql_user_password

}
*/
#########################################################################
#                           Compute - Instances                         #
#########################################################################
module "compute_instances" {
  source                                      = "../../resources/gcp/compute_instances/"
  # Single Instances data
  role_compute_admin_id                       = module.iam_api.role_compute_admin_id
  instance_demo_dashboard_type                = var.instance_demo_dashboard_type
  instance_demo_dashboard_zone                = var.instance_demo_dashboard_zone
  instance_demo_dashboard_network             = module.network.demo_dashboard_network_name
  # Instances
  instance_group_demo_dashboard_name          = var.instance_group_demo_dashboard_name
  demo_dashboard_app_target_tags              = var.demo_dashboard_app_target_tags
  demo_dashboard_ssh_target_tags              = var.demo_dashboard_ssh_target_tags
  demo_dashboard_http_https_target_tags       = var.demo_dashboard_http_https_target_tags

  app_engine_code_bucket_name                 = module.storage.app_engine_code_bucket_name
  app_engine_code_bucket_object_1_name        = module.storage.app_engine_code_bucket_object_1_name
}

#########################################################################
#                      Data - SQL Databases (MySQL)                     #
#########################################################################
module "data_sql" {
  source                                         = "../../resources/gcp/data_sql/"
  # CloudSQL - demo_dashboard_mysql
  api_cloud_sql                                  = module.iam_api.api_cloud_sql
  api_service_networking                         = module.iam_api.api_service_networking
  # DB Instance
  demo_dashboard_mysql_database_instance_name    = var.demo_dashboard_mysql_database_instance_name
  demo_dashboard_mysql_database_version          = var.demo_dashboard_mysql_database_version
  demo_dashboard_mysql_database_region           = var.demo_dashboard_mysql_database_region
  demo_dashboard_mysql_database_tier             = var.demo_dashboard_mysql_database_tier
  demo_dashboard_mysql_database_ipv4_enabled     = var.demo_dashboard_mysql_database_ipv4_enabled
  demo_dashboard_mysql_database_private_network  = module.network.demo_dashboard_network_id
  # Database
  demo_dashboard_mysql_database_name             = var.demo_dashboard_mysql_database_name
  # Users
  demo_dashboard_mysql_sql_user_name             = var.demo_dashboard_mysql_sql_user_name
  demo_dashboard_mysql_sql_user_password         = var.demo_dashboard_mysql_sql_user_password
}

#########################################################################
#                                Services                               #
#########################################################################
module "services" {
  source                                          = "../../resources/gcp/services/"
  role_compute_admin_id                           = module.iam_api.role_compute_admin_id
  api_compute                                     = module.iam_api.api_compute
  demo_dashboard_app_id                           = module.security.demo_dashboard_app_id
  demo_dashboard_http_https_id                    = module.security.demo_dashboard_http_https_id
  demo_dashboard_ssh_id                           = module.security.demo_dashboard_ssh_id
  demo_dashboard_instance_group_manager_id        = module.compute_instances.demo_dashboard_instance_group_manager_id
  demo_dashboard_fwd_id                           = module.network_load_balancer.demo_dashboard_fwd_id
  demo_dashboard_tp_id                            = module.network_load_balancer.demo_dashboard_tp_id
  demo_lb_a_record_id                             = module.domain.demo_lb_a_record_id
  demo_dashboard_http_forwarding_rule_id          = module.https_load_balancer.demo_dashboard_http_forwarding_rule_id
}

