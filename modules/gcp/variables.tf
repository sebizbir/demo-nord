#########################################################################
#                                Default                                #
#########################################################################
variable "demo_project_id" {
  description = "Project ID"
  default     = "nordcloud-266711"
}

variable "demo_project_region" {
  default = "europe-west2"
}

variable "demo_project_number" {
  description = "Project Number"
  default      = "863254534554"
}
#########################################################################
#                                Network                                #
#########################################################################
#########################################################
#                          VPC                          #
#########################################################
variable "network_name" {
  description = "Name for VPC network"
  default     = "demo-dashboard"
}

variable "vpc_subnetworks" {
  default = true
}

variable "env_id" {
  description   = "Environment name"
  default       = "demo"
}

variable "enable_ssl" {
  description = "true/false value for enable SSL"
  type = bool
  default = false
}
#########################################################
#                        Firewall                       #
#########################################################
variable "demo_dashboard_http_https_allow_protocol" {
  default = "TCP"
}
variable "demo_dashboard_http_https_allow_protocol_ports" {
  default = ["80","443"]
}
variable "demo_dashboard_http_https_target_tags" {
  default = "http-https-server"
}
variable "demo_dashboard_http_https_source_ranges" {
  default = "0.0.0.0/0"
}

variable "demo_dashboard_ssh_allow_protocol" {
  default = "TCP"
}
variable "demo_dashboard_ssh_allow_protocol_ports" {
  default = ["22"]
}
variable "demo_dashboard_ssh_target_tags" {
  default = "ssh-server"
}
variable "demo_dashboard_ssh_source_ranges" {
  default = "0.0.0.0/0"
}

variable "demo_dashboard_app_allow_protocol" {
  default = "TCP"
}
variable "demo_dashboard_app_allow_protocol_ports" {
  default = ["5000"]
}
variable "demo_dashboard_app_target_tags" {
  default = "app-server"
}
variable "demo_dashboard_app_source_ranges" {
  default = "0.0.0.0/0"
}
#########################################################################
#                                DOMAIN                                 #
#########################################################################
variable "gcp_domain_name" {
  description = "Domain name for a GCP project and SSL certificate."
  default     = ""
}
#########################################################################
#                                Compute                                #
#########################################################################
#########################################################
#                       Instances                       #
#########################################################
# Single instances
variable "instance_demo_dashboard_name" {
  default = "demo"
}
variable "instance_demo_dashboard_type" {
  default = "f1-micro"
}
variable "instance_demo_dashboard_zone" {
  default = "europe-west2-a"
}

# Instance groups
variable "instance_group_demo_dashboard_name" {
  default = "demo"
}

#########################################################
#                       CloudRun                       #
#########################################################
variable "cloud_run_demo_dashboard_service_name" {
  default = "demo"
  description = "Name of Cloud Run service"
}
variable "cloud_run_demo_dashboard_service_location" {
  default = "europe-west1"
  description = "Region for Cloud Run service (europe-west2 not supported)"
}
variable "cloud_run_demo_dashboard_service_namespace" {
  default = ""
  description = "Namespace must be equal to either the project ID or project number"
}
variable "cloud_run_demo_dashboard_service_image" {
  description = "Name of the docker image to deploy (url from gcr)."
  default = ""
}
#########################################################################
#                                Storage                                #
#########################################################################
#########################################################
#                        Buckets                        #
#########################################################

variable "app_engine_code_bucket_name" {
  description = "Name for a bucket, should be same as hosted zone for Cloud DNS"
  default = "gcp-demo-dashboard-app-engine-code"
}
variable "app_engine_code_bucket_location" {
  description = "The GCS location"
  default = "EU"
}
variable "app_engine_code_bucket_class" {
  description = "Name for a bucket, should be same as hosted zone for Route53: variable domain_name"
  default = "STANDARD"
}

#########################################################################
#                             Data - SQL                                #
#########################################################################
#########################################################
#                          MySQL                        #
#########################################################
# CloudSQL - demo_dashboard_mysql
# DB Instance
variable "demo_dashboard_mysql_database_instance_name" {
  default = "demo-dashboard-master-instance"
}
variable "demo_dashboard_mysql_database_version" {
  default = "MYSQL_5_7"
}
variable "demo_dashboard_mysql_database_region" {
  default = "europe-west1"
}
variable "demo_dashboard_mysql_database_tier" {
  default = "db-f1-micro"
}
variable "demo_dashboard_mysql_database_ipv4_enabled" {
  default = true
}
# Database
variable "demo_dashboard_mysql_database_name" {
  default = "demo-dashboard-wordpress"
}
# Users
variable "demo_dashboard_mysql_sql_user_name" {
  default = "demo-admin"
}
variable "demo_dashboard_mysql_sql_user_password" {
  default = "R@nd0m"
}
