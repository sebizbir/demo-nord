provider "google" {
  credentials = file("~/.gcp/nordcloud-266711-0faa71d7f6a3.json")
  project     = var.demo_project_id
  region      = var.demo_project_region
  version = ">= 3.3"
}

provider "google-beta" {
  credentials = file("~/.gcp/nordcloud-266711-0faa71d7f6a3.json")
  project     = var.demo_project_id
  region      = var.demo_project_region
}

