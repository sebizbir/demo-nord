# Scripts with modules (reusable infrastructure)
# Demo is ready for GCP, for AWS only draft is available
Repository for terraform config files

## Prerequisites
To use this repo you have to ensure all of the values passed into the initilisation jobs and gitlab variables are correct + that your access keys are generated and stored as SECURE variables

## Info

Terraform scripts wre split into 2 folders based on provisioning type - modules and resources. Each folder consists of cloud service type: `aws`, `gcp`
And on a lower level resources folder consists of folders with module types:  `network`, `security`, `compute` etc.

#### Resources can be created as separated modules using targets or created using `services` module. `Services` module consists of `null_resources` which launch chain of blocks which are interconnected and form unified services

##### Modules:
Files - modules:

| File | Description |
| --- | --- |
| `plans` | Folder which stores creation time for null_resources |
| `main.tf` | Modules creation |
| `providers.tf` | Provider definition |
| `variables.tf` | Variables/parameters for resources |
| `terraform.tfvars` | Variables |
| `terraform.tfstate` | Current state of the environment |
| `terraform.tfstate.backup` | Current state of the environment (backup) |

##### Resources:
Files - resources:

| File | Description |
| --- | --- |
| `variables.tf` | Variables/parameters for resources |
| `outputs.tf` | Outputs from resources to use in other modules |
| `*service*.tf` | Services which will be created in a module |

## Useful Documentation
[Terraform AWS Provider Documentation](https://www.terraform.io/docs/providers/aws/)

[Terraform GCP Provider Documentation](https://www.terraform.io/docs/providers/google/index.html)

# Demo Instruction

## GCP modules structure


| Module | Description |
| --- | --- |
| `compute_app_engine` | -draft- for possible App Engine application |
| `compute_instances` | Instance groups with startup script, which fetches data from GCS and starts the application |
| `compute_run` | -draft- for possible Cloud Run application |
| `data_sql` | -draft- for possible MySQL database |
| `domain` | Cloud DNS domain, hosted zone and records for LB or //commented// Cloud Run records |
| `https_load_balancer` | Load balancer for port 80 and possible 443 with google SSL |
| `iam_api` | IAM roles and API's |
| `network` | Creation of VPC |
| `network_load_balancer` | Network Load balancer for port 5000 |
| `security` | FW records for 22, 80, 443 and 5000 from 0.0.0.0/0 |
| `services` | null_resources for starting chains |
| `storage` | GCS which held flask code, creating zip archive first |

### GCP demo

1. Create a project in Google Cloud Console
2. Review and add your data in `variables.tf` file. Such information as demo_project_id, demo_project_region and demo_project_number is required
3. Add path to your credentials file in `providers.tf` file
4. Go into IAM console of your ptoject and create service account for Terraform with JSON key (account should initially have Security Admin role, Service Account Admin)
5. Enable API's: Identity and Access Management (IAM) API, Cloud Resource Manager API
6. Go into modules/gcp folder and runn `terraform init`
7. Run `terraform apply -target module.iam_api -auto-approve` to enable all required API's and required IAM roles (there can be an error because of in-progress creation of @serverless-robot-prod.iam.gserviceaccount.com accounts, ignore it and run once again)
8. Resources can be created as separated modules using targets or created using `services` module

#### If you want to run on port 5000 and/or use NLB set host and port definition in runserver.py:

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5000)


- to launch an application on Compute Engine (in presented Terraform code with instance groups), accessible by its IP:5000 

`terraform plan -target module.services.null_resource.demo_on_compute_engine`

`terraform apply -target module.services.null_resource.demo_on_compute_engine -auto-approve`

- to launch an application on Compute Engine + Network Load Balancer, accessible by NLB IP:5000

`terraform plan -target module.services.null_resource.demo_on_compute_engine_with_nlb -target module.network_load_balancer`

`terraform apply -target module.services.null_resource.demo_on_compute_engine_with_nlb -target module.network_load_balancer -auto-approve`

- to launch an application on Compute Engine + Network Load Balancer + Cloud DNS, accessible by NLB IP:5000

`terraform plan -target module.services.null_resource.demo_on_compute_engine_with_nlb_and_domain -target module.network_load_balancer`

`terraform apply -target module.services.null_resource.demo_on_compute_engine_with_nlb_and_domain -target module.network_load_balancer -auto-approve`


#### If you want to run on port 80 and/or use HTTPS LB set host and port definition in runserver.py:

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=80)

- to launch an application on Compute Engine (in presented Terraform code with instance groups), accessible by its IP 

`terraform plan -target module.services.null_resource.demo_on_compute_engine`

`terraform apply -target module.services.null_resource.demo_on_compute_engine -auto-approve`

- to launch an application on Compute Engine + HTTPS Load Balancer, accessible by NLB IP

`terraform plan -target module.services.null_resource.demo_on_compute_engine_with_http_lb -target module.https_load_balancer`

`terraform apply -target module.services.null_resource.demo_on_compute_engine_with_http_lb -target module.https_load_balancer -auto-approve`

- to launch an application on Compute Engine + HTTPS Load Balancer + Cloud DNS, accessible by NLB IP

`terraform plan -target module.services.null_resource.demo_on_compute_engine_with_http_lb_and_domain -target module.https_load_balancer`

`terraform apply -target module.services.null_resource.demo_on_compute_engine_with_http_lb_and_domain -target module.https_load_balancer -auto-approve`

9. To destroy created resources run following set of commands: 

`terraform destroy -target module.network -target module.security -target module.storage -target module.domain -target module.compute_instances -target compute.network_load_balancer -target module.https_load_balancer -auto-approve`

10. Run `terraform destroy -target module.iam_api -auto-approve` to finally disable IAM roles that were enabled for service account
11. Optionally data_sql module is working. For Cloud DNS usage, you should first enter it as a variable in `variables.tf` and confirm that your terraform service account is added as one of the users/admins of chosen domain.

## AWS modules structure

1. Review and add your data in `variables.tf` file. Such information as demo_project_id, demo_project_region and demo_project_number is required
2. Add path to your ACCESS KEYS file in `providers.tf` file or EXPORT it as a `terraform` profile

| Module | Description |
| --- | --- |
| `compute_beanstalk` | -draft- for possible Beanstalk application |
| `compute_ec2` | -draft- Instance groups with startup script, which fetches data from GCS and starts the application |
| `compute_ecs` | -draft- for possible ECS application |
| `data_sql` | Creation of possible MySQL RDS database |
| `domain` | -draft- Route53 domain, hosted zone |
| `iam` | IAM roles |
| `network` | Creation of VPC, NAT, subnets and route tables |
| `repositories` | Creation of ECR |
| `security` | Creation of Security Groups |
| `storage` | Creation of S3 which held flask code, creating zip archive first  |
